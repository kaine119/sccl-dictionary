import Vue from 'vue';
import Router from 'vue-router';
import Search from '@/views/Search.vue';
import Definition from '@/views/Definition.vue';
import OcrResults from '@/views/OcrResults.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'search',
      component: Search,
    },
    {
      path: '/definition/:id',
      name: 'definition',
      component: Definition,
      props: true,
    },
    {
      path: '/ocr-results',
      name: 'ocr-results',
      component: OcrResults,
    },
  ],
});
