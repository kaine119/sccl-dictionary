import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentImage: null,
  },
  /* eslint-disable no-param-reassign */
  mutations: {
    changeFile(state, { newFile }) {
      state.currentImage = newFile;
    },
  },
  /* eslint-enable */
  getters: {
    currentImage: state => state.currentImage,
  },
});
