import axios from 'axios';

export function fetchQueryResults(query) {
  return axios.get(`/api/result/${query}`)
    .then(res => new Promise((resolve) => {
      const { data: { words: results } } = res;
      const toReturn = results.map(result => ({
        id: result.wordId,
        word: result.word,
        pos: result.POS,
        isStarred: result.isStarred,
      }));

      resolve(toReturn);
    }));
}

export function fetchDefinitionByID(id) {
  return axios.get(`/api/definition/${id}`)
    .then(res => new Promise((resolve) => {
      const {
        data: {
          word, wordDef, isStarred, imageURL,
        },
      } = res;
      resolve({
        id,
        word,
        definition: wordDef,
        imageURL,
        isStarred,
      });
    }));
}

export function postImageQuery(encodedImage) {
  return axios.post('/api/ocr', { image: encodedImage })
    .then(results => new Promise((resolve) => {
      console.log(results)
      const toReturn = results.data.map(result => ({
        id: result.wordId,
        word: result.word,
        pos: result.POS,
        isStarred: result.isStarred,
      }));

      resolve(toReturn);
    }));
}

export function postDefinitionToggleStar(id) {
  return axios.get(`/api/toggle/${id}`);
}

export function postFeedback({ body }) {
  return axios({
    url: '/api/feedback',
    method: 'post',
    data: {
      feedback_content: body,
    },
  });
}
