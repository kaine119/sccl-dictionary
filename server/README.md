# Dictionary server

This will have a few endpoints to:
- get dictionary entries
- post feedback form entries (#7)
- let a user login and have their token saved in their session (#13)
- post usage details for a user (#13)

For Google Cloud Vision api, use key json file to authenticate

export GOOGLE_APPLICATION_CREDENTIALS=PATH_TO_KEY_FILE
Replace PATH_TO_KEY_FILE with the path to your JSON service account file. GOOGLE_APPLICATION_CREDENTIALS should be written out as-is (it's not a placeholder in the example above).