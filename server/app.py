from flask import Flask, redirect, render_template, request, url_for, session, jsonify, make_response, abort
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import NVARCHAR, desc, func
from flask_oauthlib.client import OAuth
from datetime import datetime
import os


app = Flask(__name__, static_url_path='')
app.config["DEBUG"] = True

app.secret_key = 'any random string'

# currently local
SQLALCHEMY_DATABASE_URI = os.environ["DATABASE_URL"]

app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_POOL_RECYCLE"] = 299
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

# Import models by name, so you don't have to prefix with models.*
from models import *
from ocr_method import *

"""
Google OAuth
project sccl-dict
"""
app.config['GOOGLE_ID'] = "1067891564183-gqpd6p85khk02o8t7cahjt44153vbsfk.apps.googleusercontent.com" 
app.config['GOOGLE_SECRET'] = "RBF8sQp8ugVheC0RSjYlieKp"
app.debug = True
app.secret_key = 'development'

oauth = OAuth(app)
google = oauth.remote_app(
    'google',
    consumer_key=app.config.get('GOOGLE_ID'),
    consumer_secret=app.config.get('GOOGLE_SECRET'),
    request_token_params={
        'scope': 'email'
    },
    base_url='https://www.googleapis.com/oauth2/v1/',
    request_token_url=None,
    access_token_method='POST',
    access_token_url='https://accounts.google.com/o/oauth2/token',
    authorize_url='https://accounts.google.com/o/oauth2/auth',
)


# main / login using Googel OAuth
@app.route('/')
def index():
    if 'google_token' in session:
        return app.send_static_file('index.html')

    # otherwise ask the user to log in
    # store user info in db, as well as as cookies
    return redirect(url_for('login'))


@app.route('/login')
def login():
    return google.authorize(callback=url_for('authorized', _external=True))


# logout page > return to login
# NOTE: only works if the user is *changing accounts*
# if the user only has one google account, it'll 
# - redirect to google's login page,
# - IMMEDIATELY sign in as the current single user, and
# - reauthenticate and log right back in.
@app.route('/logout')
def logout():
    session.pop('google_token', None)
    session.pop('curr_user_id', None)
    return redirect(url_for('index'))


@app.route('/login/authorized')
def authorized():
    # get the response from the OAuth request.
    oauth_response = google.authorized_response()
    # if there isn't any response (for some reason?) throw the user an error.
    if oauth_response is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        )

    # keep the OAuth token in session for future OAuth requests
    session['google_token'] = (oauth_response['access_token'], '')


    # info abt. the user
    user_is_me = google.get('userinfo')
    
    # grab info from the userinfo object
    user_id = user_is_me.data["id"] # the pri. key stored in db
    user_email = user_is_me.data["email"]
    user_name = user_is_me.data["name"]
    user_family_name = user_is_me.data["family_name"]
    user_given_name = user_is_me.data["given_name"]

    # store user info in our db
    existing_user = User.query.filter_by(_id=user_id).first()
    if existing_user is None:
        # add new user to db
        new_user = User(
            _id=user_id, 
            email=user_email, 
            name=user_name, 
            family_name=user_family_name, 
            given_name=user_given_name)

        db.session.add(new_user)
        try:
            db.session.commit()
        except:
            db.session.rollback()
            raise

    # keep the current user id in session for the check in /
    session['curr_user_id'] = user_id

    # construct a response to return
    # the response will redirect the user to /
    resp = make_response(redirect(url_for('index')))

    # set cookies in the response for user info
    resp.set_cookie('user_id', user_id)
    resp.set_cookie('user_email', user_email)
    resp.set_cookie('user_name', user_name)
    
    return resp


@google.tokengetter
def get_google_oauth_token():
    return session.get('google_token')


@app.route("/api/ocr", methods=["POST"])
def ocr():
    """ocr function"""

    json_object = request.json
    # {"image": <base64'd string>}
    image_64_encode = json_object['image']
    texts = ocr_api(image_64_encode)

    json_objects = []

    for text in texts:
        word_result = result(text).get_json()
        # dedupe
        if any(result['word'] == text for result in json_objects):
            continue
        json_objects.extend(word_result['words']) # since the normal result api will only result queries for one text str
        print(json_objects)

    return jsonify(json_objects)

@app.route("/api/result/<searchingword>", methods=["GET"])
def result(searchingword):
    """search result page"""
    
    # search for all possible words
    likeStr = '%' + searchingword + '%'
    
    words = Dictionary.query.filter(Dictionary.word.like(likeStr))
    serialized_words = [x.serialize_with_starred(session['curr_user_id']) for x in words]
    json_object = {"words": serialized_words}   # return the entire list of all possible words for json, then for word in words, word._id -> its id, word.word -> the word etc.

    if words is None:
        missing_word = (
            MissingWords.query
            .filter(MissingWords.word==searchingword)
            .all()
            )

        if missing_word is None:
            # add in missingwords table
            curr_time = datetime.now()
            
            new_word = MissingWords(
                word = searchingword,
                datetime = curr_time
            )

            db.session.add(missing_word)
            try:
                db.session.commit()
            except:
                db.session.rollback()
                raise

    return jsonify(json_object)

@app.route("/api/history")
def history():
    """
    search history page
    recent max 10 words
    newest in front
    """

    curr_user_id = session['curr_user_id']
    history_words = (
                    UserHistory.query
                    .filter(UserHistory.userId==curr_user_id)
                    .group_by(UserHistory.wordId)
                    .order_by(desc(UserHistory.datetime))
                    .limit(10)
                    .all()
                    )
    words_id = []
    for word in history_words:
        words_id.append(word.wordId)

    return jsonify({"history":words_id})  # words_id is a dict of the recent words sorted based on datetime


@app.route('/api/toggle/<id>', methods=['GET'])
def toggle_star(id):
    """change the if starred status of a word of one user"""


    word_existed = Dictionary.query.filter(Dictionary.id==id).first()

    # word not exist
    if word_existed is None:
        abort(404)

    starred_item = UserStar.query.filter(UserStar.wordId==word_existed.id, UserStar.userId==session['curr_user_id']).first()

    if starred_item is None:
        # add in star
        new_starred = UserStar(
            wordId = id,
            userId = session['curr_user_id']
            )
        db.session.add(new_starred)
        try:
            db.session.commit()
        except:
            db.session.rollback()
            raise
    else:
        # delete the record
        db.session.delete(starred_item)
        try:
            db.session.commit()
        except:
            db.session.rollback()
            raise

    return ('', 204) # empty response


@app.route('/api/definition/<id>', methods=["GET"])
def definition(id):
    """word definition page"""

    definition_to_return = Dictionary.query.filter(Dictionary.id==id).first()

    if definition_to_return is None:
        abort(404)

    starred_item =  UserStar.query.filter(UserStar.wordId==definition_to_return.id, UserStar.userId==session['curr_user_id']).first()

    if starred_item is not None:
        is_starred = True
    else:
        is_starred = False

    print(definition_to_return.imagePath)
    image_exist = (definition_to_return.imagePath == 1) # boolean
    if image_exist:
        image_url = "https://storage.googleapis.com/sccl-dictionary-assets/definition-images/{}.png".format(id)
    else:
        image_url = ""

    # return the id, word itself, definition for json, & if starred by the user
    json_object = {
        "word_id": definition_to_return.id,
        "word": definition_to_return.word,
        "wordDef":  definition_to_return.definitionUser,
        "isStarred": is_starred,
        "imageURL": image_url
    }    

    # record the word in UserHistory
    curr_user_id = session['curr_user_id']
    curr_time = datetime.now()

    words_num = UserHistory.query.filter(UserHistory.userId==curr_user_id).count()
    
    word_searched = UserHistory.query.filter(UserHistory.userId==curr_user_id,
                                             UserHistory.wordId==id).first()
    
    words = UserHistory.query.filter(UserHistory.userId==id).all()
    
    new_word_searched = UserHistory(
            wordId=definition_to_return.id,
            userId=curr_user_id,
            datetime=curr_time
            )
    db.session.add(new_word_searched)
    try:
        db.session.commit()
    except:
        db.session.rollback()
        raise



    # TODO: replace this
    # word history count can be achieved with SELECT COUNT(id)
    # or the equivalent in SQLAlchemy, query.count()

    # record the word in WordHistory
    # word = WordHistory.query.filter(WordHistory._id==id).first()
    # if word is None:
    #     # add a new search record
    #     count = 1
    #     new_word_searched = WordHistory(id, count)
    #     db.session.add(new_word_searched)
    #     db.session.commit()
    # else:
    #     word.count = word.count + 1
    #     db.session.commit()

    return jsonify(json_object)

# TODO: stub
@app.route('/api/feedback', methods=['POST'])
def feedback():
    """add feedback to the table"""

    feedback = request.json
    print(feedback)
    feedback_title = None
    feedback_content = feedback['feedback_content']
    user = session['curr_user_id']

    new_feedback =  Feedback(
        userId = user,
        title = feedback_title,
        content = feedback_content 
        )

    db.session.add(new_feedback)
    try:
        db.session.commit()
    except:
        db.session.rollback()
        raise

    return ('', 204) # empty response

@app.route('/admin', methods=["GET"])
def admin():
    if 'curr_user_id' in session:
        is_admin = User.query.filter(session['curr_user_id'] == User._id, User.is_admin == 1).first()
        if is_admin:
            return app.send_static_file('admin/index.html')
        else:
            return ('', 403)
    else:
        return redirect(url_for('login'))

@app.route('/api/tracking', methods=["GET"])
def tracking():
    """
    for the admin to track web app usage
    things to show:
        10 most frequently searched words
        words searched but not in the dictionary
        feedback (content only)
    """

    # most frequent words
    # in SQL: SELECT wordId, count(wordId) GROUP BY wordId;
    # returns an array of tuples (word_id, count)
    frequent_words_queries = (
                    db.session.query(UserHistory.wordId, func.count(UserHistory.wordId))
                    .group_by(UserHistory.wordId)
                    .order_by(func.count(UserHistory.wordId))
                    .all()
                    )
    print(frequent_words_queries)
    frequent_words = []
    for frequent_word_query in frequent_words_queries:
        word_id, count = frequent_word_query
        word_query = (
            Dictionary.query
            .filter(Dictionary.id == word_id)
            .first()
            )
        frequent_words.append({ "word": word_query.word, "count": count })

    # feedback from users
    feedback_content = []
    feedback_queries = (
        Feedback.query
        .all()
        )
    for feedback_query in feedback_queries:
        feedback_content.append(feedback_query.content)

    # missing words
    missing_words = []
    missing_word_queries = (
        MissingWords.query
        .all()
        )

    for missing_word_query in missing_word_queries:
        word_dict = {
            "word":missing_word_query.word,
            "datetime": missing_word_query.datetime
            }
        missing_words.append(word_dict)

    tracking_object = {
        "most_frequent_words": frequent_words,
        "feedback": feedback_content,
        "missing_words": missing_words
    }

    res = make_response(jsonify(tracking_object))
    res.headers.add('Access-Control-Allow-Origin', '*')
    res.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    res.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')

    return res


if __name__ == "__main__":
    app.run(port=5050)