"""
models for db tables
by Yiyang
"""

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import NVARCHAR

db = SQLAlchemy()

"""
tables for the dictionary contents,
provided by SCCL
"""
class Xianhan(db.Model):
    """
    definition from xianhan
    'xianhan' table
    """

    __tablename__ = "xianhan"

    id = db.Column(db.Integer, primary_key=True)
    word = db.Column(db.Text)
    POS = db.Column(db.Text)
    pinYin = db.Column(db.Text)
    definition = db.Column(db.Text)
    TRPWordId = db.Column(db.Integer)
    active = db.Column(db.Integer)  # boolean

class Sentences(db.Model):
    """
    example sentences
    'sentences' table
    """

    __tablename__ = "sentences"

    id = db.Column(db.Integer, primary_key=True)
    previousSentence = db.Column(db.Text)
    senntence = db.Column(db.Text)
    nextSentence = db.Column(db.Text)
    grade = db.Column(db.Integer)
    stream = db.Column(db.Text)
    lesson = db.Column(db.Integer)
    filePath = db.Column(db.Text)
    source = db.Column(db.Text)

class Yuyi(db.Model):
    """
    yuyi (meaning) of words in sentences
    'yuyi' table
    """

    __tablename__ = "yuyi"

    id = db.Column(db.Integer, primary_key=True)
    parentId = db.Column(db.Integer)
    label = db.Column(db.Text)
    text = db.Column(db.NVARCHAR)
    description = db.Column(db.NVARCHAR)
    _type = db.Column(db.Text)      # POS? tbc
    active = db.Column(db.Integer)  # boolean


class Dictionary(db.Model):
    """
    the definition etc. table
    'main' table of the provided .xls file for data
    """

    __tablename__ = "dictionary"

    id = db.Column(db.Integer, primary_key=True)
    word = db.Column(db.Text())
    POS = db.Column(db.Text())
    pinYin = db.Column(db.Text())
    pinYinNumber = db.Column(db.Integer)    # pinyin with no.

    yuYiId = db.Column(db.Integer)
    sentenceId = db.Column(db.Integer)

    paragraphIdx = db.Column(db.Integer)
    sentenceIdx = db.Column(db.Integer)
    wordIdx = db.Column(db.Integer)
    positionIdx = db.Column(db.Integer)

    source = db.Column(db.Text())       # textbook name
    filePath = db.Column(db.Text())

    word_level =  db.Column(db.Text())  # representing the following 3
    grade = db.Column(db.Integer)
    stream =  db.Column(db.Text())
    lesson = db.Column(db.Integer)
    
    definitionXianHanId = db.Column(db.Integer)
    definitionUser = db.Column(db.Text(400))

    abnormal = db.Column(db.Text()) # boolean
    abnormalRemarks = db.Column(db.Text())

    isXianHan = db.Column(db.Integer) # boolean
    example = db.Column(db.Text())

    # for imagePath, store as boolean
    imagePath = db.Column(db.Integer())
    videoPath = db.Column(db.Text())

    tagStatusLL = db.Column(db.Integer) # boolean
    tagStatusGHH = db.Column(db.Integer) # boolean

    @property
    def serialize(self):
        """
        Returns everything needed in a serializable dict
        """
        return {
            'wordId': self.id,
            'word': self.word,
            'POS': self.POS,
            'pinYin': self.pinYin,
            'pinYinNumber': self.pinYinNumber,
            'yuYiId': self.yuYiId,
            'sentenceId': self.sentenceId,
            'definitionUser': self.definitionUser,
            'definitionXianHanId': self.definitionXianHanId
        }

    # extends serialize with another field for if the word has been starred by user_id.
    def serialize_with_starred(self, user_id):
        print(user_id)
        starred = UserStar.query.filter(
            UserStar.wordId==self.id, 
            UserStar.userId==user_id
        ).first() is not None # i.e. True if the query exists

        to_return = self.serialize
        to_return.update({'isStarred': starred})
        return to_return

"""
User info tables
"""
class User(db.Model):
    """
    user account table
    login using Google OAuth
    therefore using google token as info stored
    """

    __tablename__ = "user"

    _id = db.Column(db.Text(), primary_key=True)
    email = db.Column(db.Text())
    is_admin = db.Column(db.Integer()) # boolean

    # the following 3 may be null
    name = db.Column(db.Text())
    family_name = db.Column(db.Text())
    given_name = db.Column(db.Text())

class UserHistory(db.Model):
    """
    table storing recent words searched by users

    p.s. words (those w. def) are stored, not queries
    """

    __tablename__ = "userhistory"

    _id = db.Column(db.Integer, primary_key=True)
    wordId = db.Column(db.Integer, db.ForeignKey("dictionary._id"), nullable=False)
    userId = db.Column(db.Integer, db.ForeignKey("user._id"), nullable=False)

    datetime = db.Column(db.DateTime())

class UserStar(db.Model):
    """
    table for the words starred by the user
    """

    __tablename__ = "userstar"

    _id = db.Column(db.Integer, primary_key=True, )
    wordId = db.Column(db.Integer, db.ForeignKey("dictionary._id"), nullable=False)
    userId = db.Column(db.Integer, db.ForeignKey("user._id"), nullable=False)

class WordHistory(db.Model):
    """
    table storing no. of times each word is searched
    """

    __tablename__ = "wordhistory"

    _id = db.Column(db.Integer, db.ForeignKey("dictionary._id"), primary_key=True)
    count = db.Column(db.Integer)

class Feedback(db.Model):
    """
    table for the feedback from the users
    """

    __tablename__ = "feedback"

    _id = db.Column(db.Integer, primary_key=True)
    userId = db.Column(db.Integer, db.ForeignKey("user._id"), nullable=False)
    # max length for feedback title & content 200 & 1000 char respectively
    title = db.Column(db.Text(200))
    content = db.Column(db.Text(1000))

class MissingWords(db.Model):
    """
    table for words users try to search but not in the database
    """

    __tablename__ = "missingwords"

    _id = db.Column(db.Integer, primary_key=True)
    word = db.Column(db.Text())
    datetime = db.Column(db.DateTime())
    
