import io
import os

import base64

# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types


def ocr_api(image_64_encode):
    """
    take base64 str of image,
    decode & pass to Google Cloud Vision api for OCR processing
    return an array of texts recognised
    """

    # Instantiates a client
    client = vision.ImageAnnotatorClient()
    
    # image_64_encode base64 string

    # convert to image
    image = open("image_tmp.png", "wb")
    image_64_decode = base64.b64decode(image_64_encode)
    image.write(image_64_decode)
    image.close()

    file_name = os.path.join(
        os.path.dirname(__file__),
        'image_tmp.png')

    # Loads the image into memory
    with io.open(file_name, 'rb') as image_file:
        content = image_file.read()

    image = types.Image(content=content)

    # Performs text detection on the image file
    response = client.text_detection(image=image)
    texts = response.text_annotations

    text_contents = []
    for text in texts:
        text_contents.append(text.description.strip())

    return text_contents

